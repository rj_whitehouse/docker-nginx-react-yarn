# docker-react-yarn

Running a basic React site through Docker, NGinx and Yarn


Docker-machine start default (if connection error)
docker build . -t docker-react
docker run -p 8080:80 docker-react
docker-machine ip
http://{replace_ip_ip_returned_above}:8080/
